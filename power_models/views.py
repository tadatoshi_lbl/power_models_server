from django.shortcuts import render
from rest_framework import status
#from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
import os

# TODO: Extract to a separate class:
from power_models_wrapper.optimal_power_flow import OptimalPowerFlow
from power_models_wrapper.formulation_type import FormulationType
from power_models_wrapper.remote_off_grid import RemoteOffGrid

from .serializers import PowerModelSerializer
from .serializers import OffGridMicrogridSerializer

#@api_view(['POST'])
#def solve_optimal_power_flow(request):
    #data = request.data
    #uploaded_file = request.FILES['excel']
    #input_file_content = request.POST['content']

class PowerModelView(APIView):

    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):

        power_model_serializer = PowerModelSerializer(data = request.data)
        if power_model_serializer.is_valid():
            power_model = power_model_serializer.save()

            print("PowerModelView: post: power_model.file: %s" % (power_model.file))

            filepath = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), ("media/%(filename)s" % {'filename':power_model.file})))

            print("PowerModelView: post: filepath: %s" % (filepath))

            result = self.perform_optimization(power_model_serializer, filepath)

            #return Response(power_model_serializer.data, status = status.HTTP_201_CREATED)
            return Response(result, status = status.HTTP_201_CREATED)
        else:
            return Response(power_model_serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    # TODO: Extract to another class:
    def perform_optimization(self, power_model_serializer, filepath):

        optimal_power_flow = OptimalPowerFlow()

        # Temp:
        #filepath = os.path.abspath(os.path.join(os.path.dirname(__file__), "../media/case3.m"))
        result = optimal_power_flow.run(filepath, formulation_type = FormulationType.ac)
        #result = optimal_power_flow.run(power_model_serializer.file, formulation_type = FormulationType.ac)

        print("result in perform_optimization in PowerModelView: %s)" % (result))

        print("type(result['objective_lb']) in perform_optimization in PowerModelView: %s)" % (type(result['objective_lb'])))

        # Note: Since -inf also has a type float, which causes JSON serialization error, convert it to string:
        result['objective_lb'] = str(result['objective_lb'])
        #result['objective_lb'] = '-inf'

        print("modified result in perform_optimization in PowerModelView: %s)" % (result))

        return result

class OffGridMicrogridView(APIView):

    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):

        off_grid_microgrid_serializer = OffGridMicrogridSerializer(data = request.data)
        if off_grid_microgrid_serializer.is_valid():
            off_grid_microgrid = off_grid_microgrid_serializer.save()

            print("OffGridMicrogridView: post: off_grid_microgrid.file: %s" % (off_grid_microgrid.file))

            filepath = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), ("media/%(filename)s" % {'filename':off_grid_microgrid.file})))

            print("OffGridMicrogridView: post: filepath: %s" % (filepath))

            result = self.perform_optimization(off_grid_microgrid_serializer, filepath)

            #return Response(power_model_serializer.data, status = status.HTTP_201_CREATED)
            return Response(result, status = status.HTTP_201_CREATED)
        else:
            return Response(off_grid_microgrid_serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    # TODO: Extract to another class:
    def perform_optimization(self, off_grid_microgrid_serializer, filepath):

        remote_off_grid = RemoteOffGrid()

        result = remote_off_grid.run(filepath)

        print("result in perform_optimization in OffGridMicrogridView: %s)" % (result))

        return result
