from django.apps import AppConfig


class PowerModelsConfig(AppConfig):
    name = 'power_models'
