from rest_framework import serializers

from .models import PowerModel
from .models import OffGridMicrogrid

class PowerModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = PowerModel
        #fields = ('file', 'remark', 'timestamp')
        fields = ('file', 'timestamp')

class OffGridMicrogridSerializer(serializers.ModelSerializer):

    class Meta:
        model = OffGridMicrogrid
        #fields = ('file', 'remark', 'timestamp')
        fields = ('file', 'timestamp')
