from django.conf.urls import url
from power_models import views

from .views import PowerModelView
from .views import OffGridMicrogridView

urlpatterns = [
    #url(r'^power_models/$', views.solve_optimal_power_flow),
    url(r'^power_models/$', PowerModelView.as_view(), name = 'power-model'),
    url(r'^off_grid_microgrids/$', OffGridMicrogridView.as_view(), name = 'off-grid-microgrid'),
]
