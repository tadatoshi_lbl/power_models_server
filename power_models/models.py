from django.db import models

class PowerModel(models.Model):
    file = models.FileField(blank=False, null=False, upload_to="input_files/")
    #remark = models.CharField(max_length=20)
    timestamp = models.DateTimeField(auto_now_add=True)

class OffGridMicrogrid(models.Model):
    file = models.FileField(blank=False, null=False, upload_to="input_files/")
    #remark = models.CharField(max_length=20)
    timestamp = models.DateTimeField(auto_now_add=True)
